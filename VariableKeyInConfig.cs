﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class VariableKeyInConfig
    {
        

        public VariableKeyInConfig() { }
        public string Name { get; set; }
        public string MaxValueKey { get; set; }
        public string MinValueKey { get; set; }
        public string XPlaneMaxValueKey { get; set; }
        public string XPlaneMinValueKey { get; set; }

    }
}

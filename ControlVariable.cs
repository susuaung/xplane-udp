﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class ControlVariable
    {
        private Variable variable;
        private Deflection defaultDeflection;
        private Deflection xplaneDeflection;

        public ControlVariable(Variable variable, Deflection defaultDeflection, Deflection xplaneDeflection)
        {
            this.variable = variable;
            this.defaultDeflection = defaultDeflection;
            this.xplaneDeflection = xplaneDeflection;
        }

        public Variable Variable
        {
            get { return this.variable; }
            set { this.variable = value; }
        }

        public Deflection DefaultDeflection
        {
            get { return this.defaultDeflection; }
            set { this.defaultDeflection = value; }
        }

        public Deflection XplaneDeflection
        {
            get { return this.xplaneDeflection; }
            set { this.xplaneDeflection = value; }
        }
    }
}

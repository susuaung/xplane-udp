##UDP Client for X-Plane Flight Simulator##

###Usage###

Start aircraft in x-plane.

Run **UDPClient.exe** to start transferring control data. 

Flight test input file is needed to run the program successfully. Input file must be in **.csv** (Excel comma separated file) format.

Program loads the input file at runtime and transmit loaded data to x-plane via UDP.

It can be configured using **config.properties** file. Configuration file should be in same directory as UDPClient.exe.

The data send to x-plane is written to a text file. Look for **output{timestamp}.txt** file in directory of program.

###Configuration###

Set the IP address and port number of computer running X-plane to **server** and **port** property.

	server=203.252.161.194
	port=49000

Set the path to flight test data file to **file** property. Set **startLine** with the line number inside input file to start reading from it. Set **numberOfLines** with the total number of Lines to be read from input file.

	file="C:\test.csv"
	startLine=1
	numberOfLines=100

Set **timeStep** property in milliseconds. It is timestep for transferring data to x-plane.

	timeStep=20

Map the column names to control variables in the input file. The data values under that column will be loaded.
	
	elevator="Elevator[deg]"
	aeileron="Aeileron[Deg]"
	rudder="Rudder[deg]"
	throttle="Thrust[%]"
	flap="Flap[deg]"

Set the max and min deflection value for control variables.
	
Elevator

	max_elev=20
	min_elev=-25
	#x-plane max and min values of elevator
	x_max_elev=1
	x_min_elev=-1	

Aeileron

	max_aeil=12
	min_aeil=-20
	x_max_aeil=1
	x_min_aeil=-1

Rudder

	max_flap=2
	min_flap=0
	x_max_flap=1
	x_min_flap=0

Flap

	max_flap=2
	min_flap=0
	x_max_flap=1
	x_min_flap=0

Throttle

*Current throttle values are commented out.*

	max_throl=0
	min_throl=0
	x_max_throl=0
	x_min_throl=0


###Note###

Error handling is not implemented yet. If anything unexpected occurred, please rerun the program.
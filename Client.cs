﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace UDPClient
{
    class Client
    {

        #region initialize member variables
        private Properties properties;

        const string defaultServerIP = "203.252.161.194";
        const int defaultSeverPort = 49000;
        const string configFile = "config.properties";
        const string defualtDataFile = "test1.csv";


        private string serverIP;
        private int serverPort;

        private string dataFile;
        private int startLine;
        private int numberOfLines;
        private int timeStep;

        private ControlVariable elevatorControl;
        private ControlVariable aeilreonControl;
        private ControlVariable rudderControl;
        private ControlVariable flapControl;
        private ControlVariable throttleControl;

        private FlightTestDataParser flightDataParser;

        //private string output;
        private StreamWriter outputStreamWriter;
        FileStream outputStream;
        #endregion
       
        public Client(Properties properties)
        {
            this.properties = properties; 
        }

        public void Start()
        {
            initClient();
            loadFlightTestDataParser();
            initializeOutputStreamWriter();
            sendToXPlane();
            closeOutputStream();
            
        }
        #region Initialize Client
        private void initClient()
        {
            setServerConfiguration();
            setDataFileConfiguration();
            setControlVariables();
        }
        private void setServerConfiguration(){
            this.serverIP = properties.get("server", defaultServerIP);
            this.serverPort = properties.getInt("port", defaultSeverPort);
        }
        private void setDataFileConfiguration()
        {
            this.dataFile = properties.get("file", defualtDataFile);
            this.startLine = properties.getInt("startLine");
            this.numberOfLines = properties.getInt("numberOfLines");
            this.timeStep = properties.getInt("timeStep");
        }
        
        private void setControlVariables(){
            elevatorControl = getControlVariable(getElevatorKeyInConfig());
            aeilreonControl = getControlVariable(getAeilreonKeyInConfig());
            rudderControl = getControlVariable(getRudderKeyInConfig());
            flapControl = getControlVariable(getFlapKeyInConfig());
            //throttleControl = getControlVariable(getThrottleKeyInConfig());
            throttleControl = new ControlVariable(new Variable(properties.get("throttle")), null, null);
        }

        private ControlVariable getControlVariable(VariableKeyInConfig key)
        {
            Variable elevator = new Variable(properties.get(key.Name));
            Deflection elevDefaultDeflection = new Deflection(properties.getFloat(key.MaxValueKey), properties.getFloat(key.MinValueKey));
            Deflection elevXPaneDeflection = new Deflection(properties.getFloat(key.XPlaneMaxValueKey), properties.getFloat(key.XPlaneMinValueKey));
            return new ControlVariable(elevator, elevDefaultDeflection, elevXPaneDeflection);
        }

        private VariableKeyInConfig getElevatorKeyInConfig()
        {
            VariableKeyInConfig key = new VariableKeyInConfig();
            key.Name = "elevator";
            key.MaxValueKey = "max_elev";
            key.MinValueKey = "min_elev";
            key.XPlaneMaxValueKey = "x_max_elev";
            key.XPlaneMinValueKey = "x_min_elev";
            return key;
        }

        private VariableKeyInConfig getAeilreonKeyInConfig()
        {
            VariableKeyInConfig key = new VariableKeyInConfig();
            key.Name = "aeileron";
            key.MaxValueKey = "max_aeil";
            key.MinValueKey = "min_aeil";
            key.XPlaneMaxValueKey = "x_max_aeil";
            key.XPlaneMinValueKey = "x_min_aeil";
            return key;
        }

        private VariableKeyInConfig getRudderKeyInConfig()
        {
            VariableKeyInConfig key = new VariableKeyInConfig();
            key.Name = "rudder";
            key.MaxValueKey = "max_rudd";
            key.MinValueKey = "min_rudd";
            key.XPlaneMaxValueKey = "x_max_rudd";
            key.XPlaneMinValueKey = "x_min_rudd";
            return key;
        }
        private VariableKeyInConfig getFlapKeyInConfig()
        {
            VariableKeyInConfig key = new VariableKeyInConfig();
            key.Name = "flap";
            key.MaxValueKey = "max_flap";
            key.MinValueKey = "min_flap";
            key.XPlaneMaxValueKey = "x_max_flap";
            key.XPlaneMinValueKey = "x_min_flap";
            return key;
        }
        private VariableKeyInConfig getThrottleKeyInConfig()
        {
            VariableKeyInConfig key = new VariableKeyInConfig();
            key.Name = "throttle";
            key.MaxValueKey = "max_throl";
            key.MinValueKey = "min_throl";
            key.XPlaneMaxValueKey = "x_max_throl";
            key.XPlaneMinValueKey = "x_min_throl";
            return key;
        }
        #endregion 

        #region Load Flight Test Data Parser
        private void loadFlightTestDataParser()
        {
            flightDataParser  = new FlightTestDataParser( loadFlightData());

        }
        
        

        private Dictionary<string,string[]> loadFlightData()
        {
            FlightTestCSVFileReader csvReader = new FlightTestCSVFileReader(dataFile);
            csvReader.Load(getCSVFileColumnNames(), this.startLine, this.numberOfLines);
            return csvReader.getFlightDataInDictionary();
        }

        private string[] getCSVFileColumnNames()
        {
            string[] colNames = new string[5];
            colNames[0] = elevatorControl.Variable.Name;
            colNames[1] = aeilreonControl.Variable.Name;
            colNames[2] = rudderControl.Variable.Name;
            colNames[3] = flapControl.Variable.Name;
            colNames[4] = throttleControl.Variable.Name;
            return colNames;
        }
        #endregion

        #region Initialize OutputStreamWriter
        private void initializeOutputStreamWriter()
        {
            this.outputStream = new FileStream("output" + getTimestamp(DateTime.Now) + ".txt", FileMode.CreateNew, FileAccess.Write, FileShare.ReadWrite);
            this.outputStreamWriter = new StreamWriter(outputStream);
            Console.SetOut(outputStreamWriter);

        }
        private string getTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
        #endregion

        #region Send To XPlane
        private void sendToXPlane()
        {
            UdpClient xplane = new UdpClient(this.serverIP, this.serverPort);
            int line = 0;
            string[] throttleArrayFormat = getThrottleDataArrayFormat();
            string[] flapArrayFormat = getFlapDataArrayFormat();
            string[] elevAeilRuddArrayFormat = getElevAndAeilAndRuddDataArrayFormat();
            while (line < this.numberOfLines)
            {
                float[] throttle = getNormalizedThrottleDataArray(line, throttleArrayFormat);   
                //float[] throttle = getNormalizedDataArray(line, throttleArrayFormat, throttleControl);
                float[] flap = getNormalizedDataArray(line, flapArrayFormat, flapControl);
                float[] elevAeilRudd = getNormalizedDataArray(line, elevAeilRuddArrayFormat,
                    new ControlVariable[] { elevatorControl, aeilreonControl, rudderControl });
                XPlaneDataStreamBuilder sBuilder = new XPlaneDataStreamBuilder();
                sBuilder.AppendDataSentence(25, throttle);
                sBuilder.AppendDataSentence(11, elevAeilRudd);
                sBuilder.AppendDataSentence(13, flap);
                byte[] stream = sBuilder.Build();
                xplane.Send(stream, stream.Length);
                saveDataArraysToOutput(throttle,flap,elevAeilRudd);
                Thread.Sleep(this.timeStep);
                line++;
            }
        }
       
        

        private string[] getElevAndAeilAndRuddDataArrayFormat()
        {
            string[] arrayFormat = getInitializedStringArray(8);
            arrayFormat[0] = elevatorControl.Variable.Name;
            arrayFormat[1] = aeilreonControl.Variable.Name;
            arrayFormat[2] = rudderControl.Variable.Name;
            return arrayFormat;
        }

        private string[] getFlapDataArrayFormat()
        {
            string[] arrayFormat = getInitializedStringArray(8);
            arrayFormat[3] = flapControl.Variable.Name;
            return arrayFormat;
        }
        private string[] getThrottleDataArrayFormat()
        {
            string[] arrayFormat = getInitializedStringArray(8);
            arrayFormat[0] = throttleControl.Variable.Name;
            return arrayFormat;
        }

        private string[] getInitializedStringArray(int length)
        {
            string[] array = new string[length];
            for (int i = 0; i < array.Length; i++)
                array[i] = "";
            return array;
        }

        private float[] getNormalizedThrottleDataArray(int line, string[] arrayFormat)
        {
            float[] throttle = flightDataParser.getDataArrayAtLine(line, arrayFormat);
            throttle[0] = throttle[0] / (100.8175448f);
            return throttle;
        }
        private float[] getNormalizedDataArray(int line, string[] arrayFormat, params ControlVariable[] variableList)
        {
            float[] dataArray = flightDataParser.getDataArrayAtLine(line, arrayFormat);
            foreach (ControlVariable var in variableList)
                for (int i = 0; i < arrayFormat.Length; i++)
                    if (arrayFormat[i].Equals(var.Variable.Name) && !(String.IsNullOrEmpty(arrayFormat[i])) )
                    {
                        dataArray[i] = DeflectionNormalizer.getNormalizedValue(dataArray[i], var.DefaultDeflection, var.XplaneDeflection);
                        break;
                    }

            return dataArray;
        }

#endregion

        #region Save Data to File

        private void saveDataArraysToOutput(float[] throttleArray, float[] flapArray, float[] elevAeilRuddArray)
        {
            
            saveArrayToOutput("Elev Aeil Rudd", elevAeilRuddArray);
            saveArrayToOutput("Flap", flapArray);
            saveArrayToOutput("throttle", throttleArray);
            Console.Write("\n");
        }
        private void saveArrayToOutput(string title, float[] array)
        {
            Console.Write(title + " :\t");
            for (int i = 0; i < array.Length; i++)
                Console.Write( array[i] + "\t");
            Console.Write("\n");
        }

       
        private void closeOutputStream()
        {
            
            outputStreamWriter.Close();
            outputStream.Close();
        }
        #endregion
    }
}

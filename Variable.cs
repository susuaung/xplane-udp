﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class Variable
    {
        

        public Variable(string name) : this(name, 0) { }
        public Variable(string name, int index)
        {
            this.Name = name;
            this.Index = index;
        }
        public string Name { get; set; }
        public int Index { get; set; }

       
    }
}

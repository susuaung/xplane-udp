﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class Deflection
    {
        private float max;
        private float min;

        public Deflection()
        {

        }
        public Deflection(float max, float min)
        {
            this.max = max;
            this.min = min;
        }

        public float Max
        {
            get { return this.max; }
            set { this.max = value; }
        }

        public float Min
        {
            get { return this.min; }
            set { this.min = value; }
        }
    }
}

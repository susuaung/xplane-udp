﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class XPlaneDataStreamBuilder
    {
        byte[] _stream = new byte[1024];
        int _cIndex = 0; //currentIndex

        public XPlaneDataStreamBuilder()
        {
            appendHeader();
        }

        
        private void appendHeader()
        {
            _stream[_cIndex++] = 68; //D
            _stream[_cIndex++] = 65; //A
            _stream[_cIndex++] = 84; //T
            _stream[_cIndex++] = 65; //A
            _stream[_cIndex++] = 48; //zero

        }
        private void appendIndex( int index)
        {
            _stream[_cIndex++] = (byte)index;
            _stream[_cIndex++] = 0;
            _stream[_cIndex++] = 0;
            _stream[_cIndex++] = 0;
        }

        private void appendData(float data)
        {
            byte[] byteArray = BitConverter.GetBytes(data);
            Array.Copy(byteArray, 0, _stream, _cIndex, byteArray.Length);
            _cIndex += byteArray.Length;
        }
        public void AppendDataSentence(int index, float[] dataArray)
        {
            appendIndex(index);
            foreach (float data in dataArray)
                appendData(data);
        }

        public byte[] Build()
        {

            byte[] result = new byte[_cIndex ];
            Array.Copy(_stream, 0, result, 0, _cIndex );
            return result;
        }
    }
}

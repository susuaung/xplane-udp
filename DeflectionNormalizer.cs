﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class DeflectionNormalizer
    {
        
        
       

        public static float getNormalizedValue(float value, Deflection defaultDeflection, Deflection newDeflection)
        {
            float newMax = newDeflection.Max;
            float newMin = newDeflection.Min;
            float max = defaultDeflection.Max;
            float min = defaultDeflection.Min;
            return ( newMax - newMin) / (max - min) * (value - max) + newMax;
        }


        
        
    }
}

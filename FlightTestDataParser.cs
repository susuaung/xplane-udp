﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UDPClient
{
    class FlightTestDataParser
    {
        
       
        private Dictionary<String, String[]> dataDic;

        public FlightTestDataParser(Dictionary<String, String[]> dataDic)
        {
           
            this.dataDic = dataDic;
        }

       

        public float[] getDataArrayAtLine(int line, string[] arrayFormat)
        {
            float[] dataArray = getInitializedFloatArray(8);
            for (int i = 0; i < arrayFormat.Length; i++)
            {
                string colName = arrayFormat[i];
                if (!String.IsNullOrEmpty(colName) && dataDic.ContainsKey(colName))
                    dataArray[i] = float.Parse(dataDic[colName][line]);
                
            }
            return dataArray;
        }

        private float[] getInitializedFloatArray(int length)
        {
            float[] array = new float[length];
            for (int i = 0; i < array.Length; i++)
                array[i] = -999f;
            return array;
        }
    }
}
